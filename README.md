`go run main.go`

_The program serves on port **8080**_

The program routes:
- _GET_  /new-game 
- _POST_ /move


To run test:
    `go test -v ./...`