package game

import (
	"errors"
	"mancala/entities"
)

type Game struct {
	Board   *entities.Board
	Player  uint8
	Message string
}

func New() *Game {
	b := entities.NewBoard()
	return &Game{Board: b, Player: b.GetFirstPlayer()}
}

func (g *Game) Move(player uint8, pit uint8) error {
	if player != g.Player {
		return errors.New("it is not your turn")
	}
	if err := g.Board.CheckMoveValidation(player, pit); err != nil {
		return err
	}
	lastPitIndex, err := g.Board.Move(pit)
	if err != nil {
		return err
	}

	g.Message = ""
	if g.Board.IsLargePit(lastPitIndex) {
		g.Player = player // free turn
		g.Message = "free turn"
	} else {
		g.Player = g.Board.GetOtherPlayerIndex(player)
		captureStatus := g.Board.CheckCapturing(lastPitIndex, player)
		if captureStatus {
			g.Message = "capturing event occurred."
		}
	}

	if g.Board.Empty(lastPitIndex) {
		g.Board.StoreAllStones(player) //the game is over
		g.Message += "The game is Over"
	}
	return nil
}
