package http

import (
	"encoding/json"
	game "mancala/services"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestNewGameRoute(t *testing.T) {
	router := SetupRouter()

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/new-game", nil)
	router.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("expected 200, got %v", w.Code)
		return
	}
	g := game.New()
	b, _ := json.Marshal(g)
	if w.Body.String() != string(b) {
		t.Error("expected new game, got something else")
	}
}
