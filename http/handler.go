package http

import (
	"errors"
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	mancala "mancala/services"
)

var sessionKey = "game"

type moveRequest struct {
	Player uint8
	Pit    uint8
}

func newGameHandler(c *gin.Context) {
	session := sessions.Default(c)
	game := mancala.New()
	err := saveGameInSession(session, game)
	if err != nil {
		failedRes(c, err.Error())
		return
	}
	successfulRes(c, game)
}

func moveHandler(c *gin.Context) {
	session := sessions.Default(c)
	g := session.Get(sessionKey)
	if g == nil {
		badRequest(c, "you should create a new game before any move.")
		return
	}
	game := g.(mancala.Game)
	req := &moveRequest{}
	err := c.BindJSON(req)
	if err != nil {
		failedRes(c, "")
	}
	err = game.Move(req.Player, req.Pit)
	if err != nil {
		badRequest(c, err.Error())
		return
	}
	err = saveGameInSession(session, &game)
	if err != nil {
		failedRes(c, err.Error())
		return
	}
	successfulRes(c, game)
}

func saveGameInSession(s sessions.Session, g *mancala.Game) error {
	s.Set(sessionKey, g)
	err := s.Save()
	if err != nil {
		return errors.New("server error - custom message")
	}
	return nil
}
