package http

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

func successfulRes(c *gin.Context, response interface{}) {
	c.JSON(http.StatusOK, response)
}

func failedRes(c *gin.Context, msg string) {
	c.JSON(http.StatusBadRequest, msg)
}

func badRequest(c *gin.Context, msg string) {
	c.JSON(http.StatusBadRequest, msg)
}
