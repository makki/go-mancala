package http

import (
	"encoding/gob"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	game "mancala/services"
)

func SetupRouter() *gin.Engine {
	r := gin.Default()
	var store = cookie.NewStore([]byte("mancala"))
	r.Use(sessions.Sessions("session", store))
	gob.Register(game.Game{})

	r.GET("/new-game", newGameHandler)
	r.POST("/move", moveHandler)

	return r
}
