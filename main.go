package main

import (
	"fmt"
	"mancala/http"
)

func main() {
	fmt.Print("The manacla developing has started at 11:11!")
	r := http.SetupRouter()
	r.Run(":8080")
}
