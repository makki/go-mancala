package entities

import "errors"

type Board struct {
	Pits [14]*Pit
}

const (
	player1 = uint8(0)
	player2 = uint8(1)
)

func NewBoard() *Board {
	b := Board{Pits: [14]*Pit{}}
	for k := range b.Pits {
		isLarge := false
		if k == 6 || k == 13 {
			isLarge = true
		}

		playerIndex := player1
		if k > 6 {
			playerIndex = player2
		}
		b.Pits[k] = newPit(playerIndex, isLarge)
	}
	return &b
}

func (b *Board) Move(pitIndex uint8) (uint8, error) {
	if pitIndex >= uint8(len(b.Pits)) {
		return pitIndex, errors.New("the starting index is out of range")
	}
	startingPit := b.Pits[pitIndex]
	stones := startingPit.StoneCount
	startingPit.StoneCount = 0
	var currIndex = pitIndex
	for i := uint8(1); i <= stones; i++ {
		currIndex = (i + pitIndex) % b.pitCount()
		p := b.Pits[currIndex]
		if p.IsLarge && p.OwnerIndex != startingPit.OwnerIndex {
			i++
			stones++
			continue
		}
		p.StoneCount++
	}
	return currIndex, nil
}

func (b *Board) CheckMoveValidation(player uint8, pitIndex uint8) error {
	if pitIndex >= uint8(len(b.Pits)) {
		return errors.New("the pit index is out of range")
	}
	pit := b.Pits[pitIndex]
	if pit.IsLarge {
		return errors.New("you can not move any stone from the large pit")
	}
	if pit.OwnerIndex != player {
		return errors.New("it is not player's pit")
	}
	if pit.StoneCount == 0 {
		return errors.New("there is no stone to move")
	}
	return nil
}

func (b *Board) Empty(player uint8) bool {
	indexes := [6]int{0, 1, 2, 3, 4, 5}
	if player == player2 {
		indexes = [6]int{7, 8, 9, 10, 11, 12}
	}
	for _, v := range indexes {
		p := b.Pits[v]
		if p.StoneCount != 0 {
			return false
		}
	}
	return true
}

func (b *Board) StoreAllStones(player uint8) {
	home := b.getHomePit(player)
	for _, v := range b.Pits {
		if v.IsLarge || v.OwnerIndex != player {
			continue
		}
		home.StoneCount += v.StoneCount
		v.StoneCount = 0
	}
}

func (b *Board) GetOtherPlayerIndex(currPlayer uint8) uint8 {
	if currPlayer == player1 {
		return player2
	} else {
		return player1
	}
}

func (b *Board) GetFirstPlayer() uint8 {
	return player1
}

func (b *Board) CheckCapturing(index uint8, player uint8) bool {
	p := b.Pits[index]
	if p.StoneCount != 1 && p.OwnerIndex != player {
		return false
	}
	oppositePit := b.Pits[b.pitCount()-index-2]
	home := b.getHomePit(player)
	home.StoneCount += oppositePit.StoneCount + 1
	oppositePit.StoneCount = 0
	p.StoneCount = 0
	return true
}

func (b *Board) pitCount() uint8 {
	return uint8(len(b.Pits))
}

func (b *Board) getHomePit(player uint8) (result *Pit) {
	if player == player1 {
		result = b.Pits[6]
	} else {
		result = b.Pits[13]
	}
	return
}

func (b *Board) IsLargePit(index uint8) bool {
	p := b.Pits[index]
	return p.IsLarge
}
