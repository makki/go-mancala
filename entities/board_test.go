package entities

import (
	"testing"
)

func TestBoard_Move(t *testing.T) {
	b := NewBoard()
	inputs := []uint8{14, 5, 9, 0, 6}
	for _, v := range inputs {
		if v >= b.pitCount() {
			if !outOfRangeMove(b, v) {
				t.Error("the index input is out of range. our expected value is an error")
			}
			continue
		}
		expectedDestination := (v + b.Pits[v].StoneCount) % b.pitCount()
		lastPit, _ := b.Move(v)
		if lastPit != expectedDestination {
			t.Errorf("expected %v, got %v", expectedDestination, lastPit)
		}
	}
}

func TestBoard_CheckMoveValidation(t *testing.T) {
	inputTable := []struct {
		player uint8
		pit    uint8
	}{
		{1, 1},
		{0, 5},
		{5, 8},
		{0, 6},
		{20, 6},
	}
	b := NewBoard()
	for _, v := range inputTable {
		err := b.CheckMoveValidation(v.player, v.pit)
		if v.pit >= b.pitCount() {
			if err == nil {
				t.Error("the index input is out of range. our expected value is an error")
			}
			continue
		}
		p := b.Pits[v.pit]
		if v.player != p.OwnerIndex || p.IsLarge || p.StoneCount == 0 {
			if err == nil {
				t.Error("this move is invalid, our expected value is an error.")
			}
		}

	}
}

func TestBoard_Empty(t *testing.T) {
	b := emptyMockBoard()
	if !b.Empty(b.GetFirstPlayer()) {
		t.Error("expected true, got false")
	}
}

func outOfRangeMove(b *Board, input uint8) bool {
	_, err := b.Move(input)
	if err == nil {
		return false
	}
	return true
}

func emptyMockBoard() *Board {
	b := NewBoard()
	home := b.getHomePit(b.GetFirstPlayer())
	for _, v := range b.Pits {
		home.StoneCount += v.StoneCount
		v.StoneCount = 0
		if v.IsLarge {
			break
		}
	}
	return b
}
