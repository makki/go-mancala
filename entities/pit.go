package entities

type Pit struct {
	OwnerIndex uint8
	IsLarge    bool
	StoneCount uint8
}

func newPit(playerIndex uint8, isLarge bool) *Pit {
	stonesCount := uint8(6)
	if isLarge {
		stonesCount = 0
	}
	return &Pit{
		OwnerIndex: playerIndex,
		IsLarge:    isLarge,
		StoneCount: stonesCount,
	}
}
